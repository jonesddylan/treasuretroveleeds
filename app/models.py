from app import db

class Treasure(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    hash = db.Column(db.String(500), unique=True)
    QRCode = db.Column(db.String(500))
    email = db.Column(db.String(200))
    prize = db.Column(db.String(200))
    active = db.Column(db.Boolean)
    def activate(self):
        self.active = True
        db.session.commit()

    def email_change(self,email):
        self.email = email
        db.session.commit()