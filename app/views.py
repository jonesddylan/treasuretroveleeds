from flask import render_template, flash, redirect, url_for

from app import app, models, db
#from .forms import EmailForm

from PIL import Image
import base64
import qrcode
import io

@app.route('/')
def index():
    return render_template("home.html")


@app.route('/enter_email/<QRCode>', methods=['GET', 'POST'])
def redirectQRCode(QRCode):
    t = models.Treasure.query.filter(models.Treasure.active == False)[1]
    #print("\nT IS " + str(t.id) + str(t.active))
    if QRCode == "N6OovBV5rYfDqqvv$50c181bb14067935ccb7b626631d7f5d1fccd9108682849744a31866e42ce849":
        t.activate()
        img = qrcode.make(t.hash)
        data = io.BytesIO()
        img.save(data, "JPEG")
        encoded_img_data = base64.b64encode(data.getvalue())
        imgdata = data.getvalue()
        img_data = encoded_img_data.decode('utf-8')
        return render_template("enter_email.html", img_data = img_data, t=t)
    else:
        return render_template("incorrect.html")
