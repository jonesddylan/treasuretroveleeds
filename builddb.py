from flask import Flask
from app import db,models
from werkzeug.security import generate_password_hash

for i in range(100):
    hash = generate_password_hash("prize" + str(i))
    
    t = models.Treasure(hash = hash,
    QRCode = "260000$N6OovBV5rYfDqqvv$50c181bb14067935ccb7b626631d7f5d1fccd9108682849744a31866e42ce849",
    prize = "Christmas Dinner For Three",
    active = False)
    db.session.add(t)
db.session.commit()
